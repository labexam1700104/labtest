const express= require('express')
const db=require('../db')
const utils=require('../util')
//const cryptoJs= require('crypto-js')
const router=express.Router()

router.get('/',(request,response)=>{
    const query=`select movie_id, movie_title, movie_release_date,movie_time,director_name from Movie_Tb`
    db.pool.execute(query,(error,movies)=>{
       response.send(utils.createResult(error,movies))
    })
})

router.post('/',(request,response)=>{
 const {movie_id, movie_title, movie_release_date,movie_time,director_name} = request.body

// const encryptedPassword = String(cryptoJs.MD5(password))
 const query= `insert into Movie_Tb(movie_id, movie_title, movie_release_date,movie_time,director_name) values (?,?,?,?)`
 db.pool.execute(query,[movie_id, movie_title, movie_release_date,movie_time,director_name],(error,result)=>{
    response.send(utils.createResult(error,result))
 })
})

router.delete('/',(request,response)=>{
    const {movie_id}=request.body
    const query=`delete from Movie_Tb where movie_id=?`
    db.pool.execute(query,[movie_id],(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

module.exports=router