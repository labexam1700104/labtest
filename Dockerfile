FROM node
WORKDIR /src
COPY . .
EXPOSE 8190
CMD node server.js